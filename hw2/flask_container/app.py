#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Flask counter app
"""

from flask import Flask, Response
import redis
import os
import logging

logging.basicConfig(filename="counter_app.log", level=logging.DEBUG)

app = Flask(__name__)

@app.route('/')
def handle_get():
    with redis.Redis(host=os.getenv("REDIS_LOCAL_IP")) as r:
        try:
            if not r.exists("visitor_nbr"):
                r.set("visitor_nbr", 0)
            resp = r.incr("visitor_nbr")
            return Response('Visitor number %d' % int(resp),
                            mimetype='text/html')
        except Exception as e:
            logging.error(e.value)

if __name__ == "__main__":
    app.run(host='0.0.0.0')
