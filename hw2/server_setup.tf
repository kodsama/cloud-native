# Variables
variable "keypair_name" {
    type = string
}

variable "number_of_flask_servers" {
    default = 2
}

# Provider
provider "openstack" {}

# Network
resource "openstack_networking_network_v2" "network" {
    name = "counter-net"
    admin_state_up = "true"
}

resource "openstack_networking_subnet_v2" "subnet" {
    name = "counter-subnet"
    network_id = "${openstack_networking_network_v2.network.id}"
    cidr = "10.0.0.0/24"
}

resource "openstack_networking_router_v2" "router" {
    name = "counter-router"
    admin_state_up = "true"
    external_network_id = "df26cc5b-b122-4506-b948-a213d2b0a7d8"
}

resource "openstack_networking_router_interface_v2" "router_iface" {
    router_id = "${openstack_networking_router_v2.router.id}"
    subnet_id = "${openstack_networking_subnet_v2.subnet.id}"
}

# Security groups
resource "openstack_networking_secgroup_v2" "ssh_group" {
    name = "shh"
    description = "Open ssh port"
}

resource "openstack_networking_secgroup_rule_v2" "allow_ssh" {
    direction = "ingress"
    ethertype = "IPv4"
    protocol = "tcp"
    port_range_min = "22"
    port_range_max = "22"
    remote_ip_prefix = "0.0.0.0/0"
    security_group_id = "${openstack_networking_secgroup_v2.ssh_group.id}"
}

resource "openstack_networking_secgroup_v2" "nginx_group" {
    name = "nginx"
    description = "Open port 80 for nginx"
}

resource "openstack_networking_secgroup_rule_v2" "allow_nginx" {
    direction = "ingress"
    ethertype = "IPv4"
    protocol = "tcp"
    port_range_min = "80"
    port_range_max = "80"
    remote_ip_prefix = "0.0.0.0/0"
    security_group_id = "${openstack_networking_secgroup_v2.nginx_group.id}"
}

resource "openstack_networking_secgroup_v2" "flask_group" {
    name = "flask"
    description = "Open port 5000 for Flask"
}

resource "openstack_networking_secgroup_rule_v2" "allow_flask" {
    direction = "ingress"
    ethertype = "IPv4"
    protocol = "tcp"
    port_range_min = "5000"
    port_range_max = "5000"
    remote_ip_prefix = "10.0.0.0/24"
    security_group_id = "${openstack_networking_secgroup_v2.flask_group.id}"
}

resource "openstack_networking_secgroup_v2" "redis_group" {
    name = "redis"
    description = "Open port 6379 for Redis"
}

resource "openstack_networking_secgroup_rule_v2" "allow_redis" {
    direction = "ingress"
    ethertype = "IPv4"
    protocol = "tcp"
    port_range_min = "6379"
    port_range_max = "6379"
    remote_ip_prefix = "10.0.0.0/24"
    security_group_id = "${openstack_networking_secgroup_v2.redis_group.id}"
}

# Server instaces
resource "openstack_compute_instance_v2" "nginx_server" {
    name            = "counter-app-nginx"
    image_name      = "Ubuntu 18.04"
    flavor_id       = "a344a6b3-780e-4534-ad9a-668ea32f0235"
    key_pair        = "${var.keypair_name}"
    security_groups = ["${openstack_networking_secgroup_v2.ssh_group.name}",
                       "${openstack_networking_secgroup_v2.nginx_group.name}",
                       "default"]
    network {
        name = "${openstack_networking_network_v2.network.name}"
    }
}

resource "openstack_compute_instance_v2" "redis_server" {
    name            = "counter-app-redis"
    image_name      = "Ubuntu 18.04"
    flavor_id       = "a344a6b3-780e-4534-ad9a-668ea32f0235"
    key_pair        = "${var.keypair_name}"
    security_groups = ["${openstack_networking_secgroup_v2.ssh_group.name}",
                       "${openstack_networking_secgroup_v2.redis_group.name}",
                       "default"]
    network {
        name = "${openstack_networking_network_v2.network.name}"
    }
}

resource "openstack_compute_instance_v2" "flask_server" {
    count           = var.number_of_flask_servers
    name            = "counter-app-flask-${count.index}"
    image_name      = "Ubuntu 18.04"
    flavor_id       = "a344a6b3-780e-4534-ad9a-668ea32f0235"
    key_pair        = "${var.keypair_name}"
    security_groups = ["${openstack_networking_secgroup_v2.ssh_group.name}",
                       "${openstack_networking_secgroup_v2.flask_group.name}",
                       "default"]
    network {
        name = "${openstack_networking_network_v2.network.name}"
    }
}

# Setup volume storage
resource "openstack_blockstorage_volume_v2" "db_volume" {
    name = "counter_storage"
    size = 1
}

resource "openstack_compute_volume_attach_v2" "volume_iface" {
    instance_id = "${openstack_compute_instance_v2.redis_server.id}"
    volume_id   = "${openstack_blockstorage_volume_v2.db_volume.id}"
}

output "volume_path" {
    value = "${openstack_compute_volume_attach_v2.volume_iface.device}"
}

# Setup ip
resource "openstack_networking_floatingip_v2" "floatingip" {
    count = var.number_of_flask_servers + 2
    pool = "internet"
}

resource "openstack_compute_floatingip_associate_v2" "nginx_iface" {
    floating_ip = "${openstack_networking_floatingip_v2.floatingip[0].address}"
    instance_id = "${openstack_compute_instance_v2.nginx_server.id}"
}

resource "openstack_compute_floatingip_associate_v2" "redis_iface" {
    floating_ip = "${openstack_networking_floatingip_v2.floatingip[1].address}"
    instance_id = "${openstack_compute_instance_v2.redis_server.id}"
}

resource "openstack_compute_floatingip_associate_v2" "flask_iface" {
    count = var.number_of_flask_servers
    floating_ip = "${openstack_networking_floatingip_v2.floatingip[count.index + 2].address}"
    instance_id = "${openstack_compute_instance_v2.flask_server[count.index].id}"
}

output "local_ip" {
    value = "\n${openstack_compute_instance_v2.nginx_server.access_ip_v4}\n${openstack_compute_instance_v2.redis_server.access_ip_v4}\n${join("\n", [for i in range(var.number_of_flask_servers): openstack_compute_instance_v2.flask_server[i].access_ip_v4])}"
}

output "floating_ip" {
    value = "\n${join("\n", [for i in range(var.number_of_flask_servers+2): openstack_networking_floatingip_v2.floatingip[i].address])}"
}

# Print inventory file for ansible
resource "local_file" "inventory" {
    content = "[nginx]\n${openstack_networking_floatingip_v2.floatingip[0].address}\n[redis]\n${openstack_networking_floatingip_v2.floatingip[1].address}\n[flask]\n${join("\n", [for i in range(var.number_of_flask_servers): openstack_networking_floatingip_v2.floatingip[i + 2].address])}\n[all:vars]\nansible_python_interpreter=python3\n[redis:vars]\nvolume_path=${openstack_compute_volume_attach_v2.volume_iface.device}\n[flask:vars]\nredis_local_ip=${openstack_compute_instance_v2.redis_server.access_ip_v4}\n"
    filename = "${path.module}/inventory.cfg"
}

# Print inventory file for ansible
resource "local_file" "upstream" {
    content = "server ${join(":5000;\nserver ", [for i in range(var.number_of_flask_servers): openstack_compute_instance_v2.flask_server[i].access_ip_v4])}:5000;\n"
    filename = "${path.module}/roles/nginx/cluster.conf"
}
