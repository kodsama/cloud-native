variable "keypair_name" {
    type = string
}
variable "public_key_path" {
    type = string
}
variable "number_of_servers" {}
provider "openstack" {
    # No settings will have it pull values from env vars, make sure to run rc file
}
resource "openstack_networking_network_v2" "network" {
    name = "counter-net"
    admin_state_up = "true"
}
resource "openstack_networking_subnet_v2" "subnet" {
    name = "counter-subnet"
    network_id = "${openstack_networking_network_v2.network.id}"
    cidr = "10.0.0.0/24"
}
resource "openstack_networking_router_v2" "router" {
    name = "counter-router"
    admin_state_up = "true"
    external_network_id = "df26cc5b-b122-4506-b948-a213d2b0a7d8"
}
resource "openstack_networking_router_interface_v2" "router_iface" {
    router_id = "${openstack_networking_router_v2.router.id}"
    subnet_id = "${openstack_networking_subnet_v2.subnet.id}"
}
resource "openstack_compute_keypair_v2" "ssh_keypair" {
    name = "${var.keypair_name}"
    public_key = "${chomp(file(var.public_key_path))}"
}
resource "openstack_identity_application_credential_v3" "credentials" {
  name         = "counter-credentials"
  description  = "Credentials for counter-app"
}
output "credential_id_and_secret" {
    value = "${openstack_identity_application_credential_v3.credentials.id} ${openstack_identity_application_credential_v3.credentials.secret}"
}
resource "openstack_networking_secgroup_v2" "security_group" {
    name = "shh-and-port-5000"
    description = "Open ssh and port 5000 for Flask"
}
resource "openstack_networking_secgroup_rule_v2" "allow_5000" {
    direction = "ingress"
    ethertype = "IPv4"
    protocol = "tcp"
    port_range_min = "5000"
    port_range_max = "5000"
    remote_ip_prefix = "0.0.0.0/0"
    security_group_id = "${openstack_networking_secgroup_v2.security_group.id}"
}
resource "openstack_networking_secgroup_rule_v2" "allow_ssh" {
    direction = "ingress"
    ethertype = "IPv4"
    protocol = "tcp"
    port_range_min = "22"
    port_range_max = "22"
    remote_ip_prefix = "0.0.0.0/0"
    security_group_id = "${openstack_networking_secgroup_v2.security_group.id}"
}
resource "openstack_objectstorage_container_v1" "container_1" {
    name   = "counter-storage"
}
resource "openstack_compute_instance_v2" "server" {
    count           = var.number_of_servers
    name            = "counter-app${count.index}"
    image_name      = "counter-app-hw1"
    flavor_id       = "a344a6b3-780e-4534-ad9a-668ea32f0235"
    key_pair        = "${openstack_compute_keypair_v2.ssh_keypair.name}"
    security_groups = ["${openstack_networking_secgroup_v2.security_group.name}",
                       "default"]
    user_data       = "#!/bin/bash\nsudo -H -u ubuntu bash -c 'cd /home/ubuntu; ./start.sh &'" 
    network {
        name = "${openstack_networking_network_v2.network.name}"
    }
}
resource "openstack_networking_floatingip_v2" "floatingip" {
    count = var.number_of_servers
    pool = "internet"
}
resource "openstack_compute_floatingip_associate_v2" "floatingip_iface" {
    count = var.number_of_servers
    floating_ip = "${openstack_networking_floatingip_v2.floatingip[count.index].address}"
    instance_id = "${openstack_compute_instance_v2.server[count.index].id}"
}
output "floating_ip" {
    value = "${[for i in range(var.number_of_servers): openstack_networking_floatingip_v2.floatingip[i].address]}"
}
