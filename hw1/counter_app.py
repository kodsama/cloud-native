from swiftclient.service import SwiftService, SwiftError, SwiftUploadObject
from swiftclient import Connection
from swiftclient.multithreading import MultiThreadingManager
from keystoneauth1 import session
from keystoneauth1.identity import v3
from flask import Flask, Response
from pprint import pformat
import os
import logging

logging.basicConfig(filename="counter_app.log", level=logging.DEBUG)

def download_counter(swift, container, object_name='counter'):
    for ret in swift.download(container, [object_name]):
        logging.debug(pformat(ret))

def update_counter(object_name='counter'):
    with open(object_name, 'r') as f:
        counter = int(f.readline())
    counter += 1
    with open(object_name, 'w') as f: f.write(str(counter) + '\n')
    return counter

def upload_counter(swift, container, object_name='counter'):
    objects = [SwiftUploadObject(object_name, object_name=object_name)]
    for ret in swift.upload(container, objects):
        logging.debug(pformat(ret))

def increment_counter(swift, container, object_name='counter'):
    download_counter(swift, container, object_name)
    value = update_counter(object_name)
    upload_counter(swift, container, object_name)
    return value

def setup_swift_auth(swift):
    with open("id_secret", "r") as f:
        auth_id, auth_secret = f.readline().split(" ")
    auth = v3.ApplicationCredential(
        auth_url = "https://xerces.ericsson.net:5000/v3",
        application_credential_secret=auth_secret,
        application_credential_id=auth_id
    )
    keystone_session = session.Session(auth=auth)
    connection = Connection(session=keystone_session)
    def create_connection():
        return connection
    swift.thread_manager.__exit__(None, None, None)
    swift.thread_manager = MultiThreadingManager(
        create_connection,
        segment_threads=swift._options['segment_threads'],
        object_dd_threads=swift._options['object_dd_threads'],
        object_uu_threads=swift._options['object_uu_threads'],
        container_threads=swift._options['container_threads']
    )

container = "counter-storage"
app = Flask(__name__)

@app.route('/')
def handle_get():
    with SwiftService() as swift:
        setup_swift_auth(swift)
        try:
            return Response('Visitor number %d' % increment_counter(swift, container),
                            mimetype='text/html')
            # return Response('{"value":%d}' % increment_counter(swift, container),
                            # mimetype='application/json')
        except SwiftError as e:
            logging.error(e.value)

if __name__ == "__main__":
    app.run(host='0.0.0.0')
