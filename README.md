# Cloud Native Assignments for Group 4

## Assignment 1 - A simple service the hard way

Our [flask application](hw1/counter_app.py) uses application credentials stored in a file on the machine to connect to the object storage.

We used terraform to be able to spawn multiple instances of the image [terraform setup](hw1/setup.tf) we created for the flask application.

It seems to be running fine and updating correctly when visiting instances in a random fashion.


## Assignment 2 - A simple service the Docker way

[Here](hw2/flask_container/) we have the files to generate a docker image for the flask application.

We deploy the VMs and surrounding infrastructure with [terraform](hw2/server_setup.tf).

We configure and start the services with [this](hw2/site.yaml) ansible-playbook.

Run this command to deploy:
```
ansible-playbook -i inventory.cfg -e "ansible_ssh_private_key_file=~/.ssh/id_rsa_ericsson_xerces_group4" site.yaml
```

## Assignment 3 - A not so simple service

[Here](hw3/simulator_as_a_service.pdf) we have our project plan.
