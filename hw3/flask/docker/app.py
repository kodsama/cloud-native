#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Flask counter app
"""

from flask import Flask, render_template, flash, request, redirect, url_for, Response
import os
import pika
import uuid


def send(message):
    queue = "jobs_waiting"

    credentials = pika.PlainCredentials(os.environ["RABBIT_USER"], os.environ["RABBIT_PASSWORD"])
    connection = pika.BlockingConnection(pika.ConnectionParameters('rabbit-broker-rabbitmq-ha', 5672, '/', credentials))
    channel = connection.channel()

    channel.queue_declare(queue=queue, durable=True)

    channel.basic_publish(exchange='', routing_key=queue, body=message)
    print(" [x] Sent message {} to queue {}.".format(message, queue))
    connection.close()

def get_results(jobid):
    queue = "{}-{}".format("result", jobid)
    waiting_queue = "jobs_waiting"
    error_queue = "error_queue"
    credentials = pika.PlainCredentials(os.environ["RABBIT_USER"], os.environ["RABBIT_PASSWORD"])
    connection = pika.BlockingConnection(pika.ConnectionParameters('rabbit-broker-rabbitmq-ha', 5672, '/', credentials))
    channel = connection.channel()

    res = channel.queue_declare(queue=waiting_queue, durable=True)
    nbr_jobs_waiting = res.method.message_count

    for i in range(0, nbr_jobs_waiting):
        job_msg = channel.basic_get(waiting_queue)
        job_string = job_msg[2].decode('utf-8')
        if str(jobid) in job_string:
            connection.close()
            return ["waiting"]

    connection.close() # Return all waiting jobs to its queue

    connection = pika.BlockingConnection(pika.ConnectionParameters('rabbit-broker-rabbitmq-ha', 5672, '/', credentials))
    channel = connection.channel()

    res = channel.queue_declare(queue=error_queue, durable=True)
    nbr_error_jobs = res.method.message_count

    for i in range(0, nbr_error_jobs):
        error_msg = channel.basic_get(error_queue)
        error_string = error_msg[2].decode('utf-8')
        if str(jobid) in error_string:
            channel.basic_ack(delivery_tag=error_msg[0].delivery_tag)
            connection.close()
            return ["error"]

    connection.close() # Return all error jobs to its queue

    connection = pika.BlockingConnection(pika.ConnectionParameters('rabbit-broker-rabbitmq-ha', 5672, '/', credentials))
    channel = connection.channel()

    try:
        res = channel.queue_declare(queue=queue, passive=True)
    except pika.exceptions.ChannelClosedByBroker:
        return None

    result_queue_len = res.method.message_count

    result_list = []

    for i in range(result_queue_len):
        message = channel.basic_get(queue)
        result_string = message[2].decode("utf-8")
        result_list.append(result_string)

    connection.close()
    return result_list

def delete_job(jobid):
    result_queue = "{}-{}".format("result", jobid)
    waiting_queue = "jobs_waiting"
    delete_queue = "delete_queue"
    error_queue = "error_queue"
    credentials = pika.PlainCredentials(os.environ["RABBIT_USER"], os.environ["RABBIT_PASSWORD"])
    connection = pika.BlockingConnection(pika.ConnectionParameters('rabbit-broker-rabbitmq-ha', 5672, '/', credentials))
    channel = connection.channel()
    channel.queue_declare(queue=delete_queue, durable=True)

    jobid_exists = False

    try:
        channel.queue_declare(queue=result_queue, passive=True, durable=True)
        jobid_exists = True
    except pika.exceptions.ChannelClosedByBroker:
        connection = pika.BlockingConnection(pika.ConnectionParameters('rabbit-broker-rabbitmq-ha', 5672, '/', credentials))
        channel = connection.channel()
        res = channel.queue_declare(queue=waiting_queue, durable=True)
        nbr_jobs_waiting = res.method.message_count

        for i in range(0, nbr_jobs_waiting):
            job_msg = channel.basic_get(waiting_queue)
            job_string = job_msg[2].decode('utf-8')
            if str(jobid) in job_string:
                jobid_exists = True

        connection.close() # Put all waiting jobs back

        connection = pika.BlockingConnection(pika.ConnectionParameters('rabbit-broker-rabbitmq-ha', 5672, '/', credentials))
        channel = connection.channel()
        res = channel.queue_declare(queue=error_queue, durable=True)
        nbr_jobs_waiting = res.method.message_count

        for i in range(0, nbr_jobs_waiting):
            job_msg = channel.basic_get(error_queue)
            job_string = job_msg[2].decode('utf-8')
            if str(jobid) in job_string:
                jobid_exists = True

        connection.close()  # Put all error jobs back

        connection = pika.BlockingConnection(pika.ConnectionParameters('rabbit-broker-rabbitmq-ha', 5672, '/', credentials))
        channel = connection.channel()

    if jobid_exists:
        channel.basic_publish(exchange='', routing_key=delete_queue, body=str(jobid))

    connection.close()

    return jobid_exists

app = Flask(__name__)
app.secret_key = uuid.uuid1()

@app.route('/', methods=['GET', 'POST'])
def index():
    error = None
    if request.method == 'POST':
        if "start_sim" in request.form:
            branch = "master" if request.form["gitbranch"] == "" else request.form["gitbranch"]
            jobid = uuid.uuid1()
            if "https://github.com/" not in request.form["gitrepo"]:
                flash("Input is not a github repository", "error")
            else:
                msg = request.form["gitrepo"] + " -b " + branch + ":jobid/" + str(jobid)
                send(msg)
                flash("Sent message {} with jobid {} to server".format(request.form["gitrepo"], str(jobid)))
        elif "sim_progress" in request.form:
            jobid = request.form["jobidresults"]
            results = get_results(jobid)
            if results is None:
                flash("No progress available for job")
            elif results[0] == "waiting":
                flash("Job is still waiting to start service")
            elif results[0] == "error":
                flash("Invalid git repo or branch for job", "error")
            else:
                flash("{}/{} jobs completed for jobid {}".format(len(results)-1, str(results[0]), jobid))
        elif "sim_res" in request.form:
            jobid = request.form["jobidresults2"]
            results = get_results(jobid)
            if results is None:
                flash("No results available for job")
            elif results[0] == "waiting":
                flash("Job is still waiting to start service")
            elif results[0] == "error":
                flash("Invalid git repo or branch for job", "error")
            else:
                return Response("\n".join(results[1:]),
                                mimetype="text/txt",
                                headers={"Content-disposition":
                                         "attachment; filename=results-{}.txt".format(jobid)})
        elif "cancel_job" in request.form:
            jobid = request.form["jobidtocancel"]
            success = delete_job(jobid)
            if not success:
                flash("Job not found")
            else:
                flash("Job with id {} has been scheduled for deletion".format(jobid))
    return render_template("index.html", error=error)


# Template for later use
@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        if request.form['username'] != 'admin' or request.form['password'] != 'secret':
            error = 'Invalid credentials'
        else:
            flash('You were successfully logged in')
            return redirect(url_for('index'))
    return render_template('login.html', error=error)

# Health check
@app.route('/health')
def health():
    return ""

if __name__ == "__main__":
    app.run(host='0.0.0.0')
