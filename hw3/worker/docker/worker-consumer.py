#!/usr/bin/env python

import pika
import os
import sys
import subprocess

jobid = os.environ["JOBID"]
gitrepo = os.environ["GITREPO"]

credentials = pika.PlainCredentials(os.environ["RABBIT_USER"], os.environ["RABBIT_PASSWORD"])
connection = pika.BlockingConnection(pika.ConnectionParameters('rabbit-broker-rabbitmq-ha', 5672, '/', credentials))
channel = connection.channel()

job_queue = "job-" + jobid
result_queue = "result-" + jobid

git_string = "git clone --depth=1 " + gitrepo + " git_folder"
subprocess.check_output(git_string.split())
os.chdir("git_folder")

pip_job_string = "python2.7 -m pip install -r requirements.txt"
subprocess.check_output(pip_job_string.split())

channel.basic_qos(prefetch_count=1)

for method, properties, body in channel.consume(job_queue):
    job_string = "python2.7 " + body
    simres = subprocess.check_output(job_string.split())
    simres = simres.decode("utf-8")
    simres = body +  "\n" + simres
    channel.basic_ack(delivery_tag=method.delivery_tag)
    channel.basic_publish(exchange='', routing_key=result_queue, body=simres)

