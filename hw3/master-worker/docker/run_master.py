#!/usr/bin/env python

import pika
import os
import sys
import time
import subprocess
import shutil

class Master:

    def __init__(self):

        self.credentials = pika.PlainCredentials(os.environ["RABBIT_USER"], os.environ["RABBIT_PASSWORD"])
        self.connection = pika.BlockingConnection(pika.ConnectionParameters('rabbit-broker-rabbitmq-ha', 5672, '/', self.credentials))
        self.channel = self.connection.channel()

        self.max_busy_workers = 10
        self.is_master = False

        self.master_queue = "master_queue"
        self.channel.queue_declare(queue=self.master_queue, durable=True)

        self.inprogress_queue = "inprogress_queue"
        self.channel.queue_declare(queue=self.inprogress_queue, durable=True)

        self.waiting_queue = "jobs_waiting"
        self.channel.queue_declare(queue=self.waiting_queue, durable=True)

        self.delete_queue = "delete_queue"
        self.channel.queue_declare(queue=self.delete_queue, durable=True)

        self.error_queue = "error_queue"
        self.channel.queue_declare(queue=self.error_queue, durable=True)

    def run(self):
        #print("start")
        while True:
            #print("##########################")
            if not self.is_master:
                print("Not master")
                master_msg = self.channel.basic_get(self.master_queue)
                if master_msg[0] is not None:
                    self.is_master = True
                    print("Became master!")
            else:
                # Check if any jobs should be deleted
                deleted_jobs = self.try_delete_jobs()

                # Check inprogress queue to know what to do
                res = self.channel.queue_declare(queue=self.inprogress_queue, durable=True)
                inprogress_queue_len = res.method.message_count
                total_nbr_busy_workers = 0

                for i in range(0, inprogress_queue_len):
                    #print("loop nbr", i)
                    busy_workers = self.check_in_progress_job(total_nbr_busy_workers, deleted_jobs)
                    total_nbr_busy_workers = total_nbr_busy_workers + busy_workers

                #print("Total busy workers: {}/{}".format(total_nbr_busy_workers, self.max_busy_workers))

                if total_nbr_busy_workers < self.max_busy_workers:
                    # Get new job from waiting queue!
                    msg_waiting_job = self.channel.basic_get(self.waiting_queue)

                    if msg_waiting_job[0] is None:
                        # No message in queue, do nothing
                        pass
                    else:
                        (jobid, nbr_jobs, gitrepo) = self.handle_waiting_job(msg_waiting_job)
                        if jobid is not None:
                            nbr_workers_to_start = min(nbr_jobs, self.max_busy_workers - total_nbr_busy_workers)
                            #print("start deployment ", jobid, "with ", nbr_workers_to_start, " workers")
                            self.start_deployment(jobid, nbr_workers_to_start, gitrepo)


            time.sleep(10)

    def try_delete_jobs(self):
        res = self.channel.queue_declare(queue=self.delete_queue, durable=True)
        delete_queue_len = res.method.message_count

        deleted_jobs = []

        for i in range(0, delete_queue_len):
            delete_msg = self.channel.basic_get(self.delete_queue)
            job_id = delete_msg[2].decode('utf-8')
            print("Trying to delete job with id " + str(job_id))

            # Check if it is in the waiting queue
            tmp_connection = pika.BlockingConnection(pika.ConnectionParameters('rabbit-broker-rabbitmq-ha', 5672, '/', self.credentials))
            tmp_channel = tmp_connection.channel()

            res_wait = tmp_channel.queue_declare(queue=self.waiting_queue, durable=True)
            waiting_queue_len = res_wait.method.message_count

            for j in range(0, waiting_queue_len):
                job_msg = tmp_channel.basic_get(self.waiting_queue)
                job_string = job_msg[2].decode('utf-8')
                if str(job_id) in job_string:
                    print("Found waiting job " + str(job_id))
                    tmp_channel.basic_ack(delivery_tag=job_msg[0].delivery_tag)

            tmp_connection.close() # Put the other waiting jobs back

            # Check if it is in the error queue
            tmp_connection = pika.BlockingConnection(pika.ConnectionParameters('rabbit-broker-rabbitmq-ha', 5672, '/', self.credentials))
            tmp_channel = tmp_connection.channel()

            res_error = tmp_channel.queue_declare(queue=self.error_queue, durable=True)
            error_queue_len = res_error.method.message_count

            for j in range(0, error_queue_len):
                error_msg = tmp_channel.basic_get(self.error_queue)
                error_string = error_msg[2].decode('utf-8')
                if str(job_id) in error_string:
                    print("Found error job " + str(job_id))
                    tmp_channel.basic_ack(delivery_tag=error_msg[0].delivery_tag)

            tmp_connection.close() # Put the other error jobs back

            result_queue = "result-" + str(job_id)
            job_queue = "job-" + str(job_id)

            try:
                self.manipulate_yaml(job_id, "qwerqwer")
                delete_dep_string = "kubectl delete -f worker_deployment.yaml"
                subprocess.check_output(delete_dep_string.split())
                print("Succeeded deleting deployment for job id " + str(job_id))
            except:
                print("Failed deleting deployment for job id " + str(job_id))
            self.reset_yaml(job_id, "qwerqwer")

            self.channel.queue_declare(queue=result_queue, durable=True)
            self.channel.queue_delete(queue=result_queue)
            self.channel.queue_declare(queue=job_queue, durable=True)
            self.channel.queue_delete(queue=job_queue)

            deleted_jobs.append(job_id)
            self.channel.basic_ack(delivery_tag=delete_msg[0].delivery_tag)

        return deleted_jobs



    def check_in_progress_job(self, total_nbr_busy_workers, deleted_jobs):
        message = self.channel.basic_get(self.inprogress_queue)
        split_strings = message[2].rsplit(':', 1)
        jobid = split_strings[0]

        if jobid in deleted_jobs:
            self.channel.basic_ack(delivery_tag=message[0].delivery_tag)
            busy_workers = 0
            return busy_workers

        #print("jobid: " + jobid)
        nbr_workers = int(split_strings[1])
        #print("nbr workers: " + str(nbr_workers))
        result_queue = "result-" + jobid
        tmp_connection = pika.BlockingConnection(pika.ConnectionParameters('rabbit-broker-rabbitmq-ha', 5672, '/', self.credentials))
        tmp_channel = tmp_connection.channel()
        tmp_len_res = tmp_channel.queue_declare(queue=result_queue, durable=True)
        result_queue_len = int(tmp_len_res.method.message_count)
        #print("result queue len: " + str(result_queue_len))
        tmp_res = tmp_channel.basic_get(result_queue)
        nbr_tasks = int(tmp_res[2])
        #print("nbr tasks in result queue: " + str(nbr_tasks))
        tmp_connection.close()
        remaining_tasks = nbr_tasks - result_queue_len + 1
        busy_workers = min(remaining_tasks, nbr_workers)

        allowed_workers = self.max_busy_workers - total_nbr_busy_workers
        desired_nbr_workers = min(remaining_tasks, allowed_workers)

        if desired_nbr_workers > nbr_workers:
            #rescale deployment
            #print("Deployment needs to be re-scaled from {} workers to {}".format(nbr_workers, desired_nbr_workers))
            busy_workers = desired_nbr_workers
            nbr_workers = desired_nbr_workers

            self.manipulate_yaml(jobid, "qwerqwer")
            rescale_dep_string = "kubectl scale --replicas=" + str(desired_nbr_workers) + " -f worker_deployment.yaml"
            subprocess.check_output(rescale_dep_string.split())
            self.reset_yaml(jobid, "qwerqwer")

            self.channel.basic_ack(delivery_tag=message[0].delivery_tag)
            result_string = jobid + ":" + str(nbr_workers)
            self.channel.basic_publish(exchange='', routing_key=self.inprogress_queue, body=result_string)
        elif busy_workers == 0:
            #delete deployment
            #print("Deployment should be deleted")
            self.manipulate_yaml(jobid, "qwerqwer")
            delete_dep_string = "kubectl delete -f worker_deployment.yaml"
            #print("before")
            subprocess.check_output(delete_dep_string.split())
            #print("after")
            self.reset_yaml(jobid, "qwerqwer")
            self.channel.queue_delete(queue="job-" + str(jobid))
            self.channel.basic_ack(delivery_tag=message[0].delivery_tag)
        else:
            #Do nothing
            #print("Do nothing for this deployment!")
            self.channel.basic_ack(delivery_tag=message[0].delivery_tag)
            result_string = jobid + ":" + str(nbr_workers)
            self.channel.basic_publish(exchange='', routing_key=self.inprogress_queue, body=result_string)

        return busy_workers

    def handle_waiting_job(self, msg_waiting_job):
        msg_string = msg_waiting_job[2].decode('utf-8')
        split_strings = msg_string.rsplit(':', 1)
        git_repo = split_strings[0]
        jobid_splits = split_strings[1].rsplit('/', 1)
        jobid = jobid_splits[1]

        git_string = "git clone --depth=1 " + git_repo + " git_folder"

        try:
            subprocess.check_output(git_string.split())
            os.chdir("git_folder")

            pip_job_string = "python2.7 -m pip install -r requirements.txt"
            subprocess.check_output(pip_job_string.split())

            get_jobs_string = "python2.7 init.py"
            jobs = subprocess.check_output(get_jobs_string.split())
            jobs = jobs.decode("utf-8")
            jobs_list = jobs.splitlines()
        except:
            # Not a valid github repo + branch!
            self.channel.basic_ack(delivery_tag=msg_waiting_job[0].delivery_tag)
            self.channel.basic_publish(exchange='', routing_key=self.error_queue, body=str(jobid))
            os.chdir("..")
            if os.path.exists("git_folder"):
                shutil.rmtree("git_folder")
            return None, None, None


        job_queue = "job-" + str(jobid)
        result_queue = "result-" + str(jobid)
        self.channel.queue_declare(queue=job_queue, durable=True)
        self.channel.queue_declare(queue=result_queue, durable=True)

        self.channel.basic_publish(exchange='', routing_key=result_queue, body=str(len(jobs_list)))

        for job in jobs_list:
            self.channel.basic_publish(exchange='', routing_key=job_queue, body=job)

        self.channel.basic_ack(delivery_tag=msg_waiting_job[0].delivery_tag)
        os.chdir("..")
        shutil.rmtree("git_folder")

        return jobid, len(jobs_list), git_repo

    def start_deployment(self, jobid, nbr_workers_to_start, gitrepo):
        #print("Got to start_deployment with jobid {}, nbr workers {}, gitrepo {}".format(jobid, nbr_workers_to_start, gitrepo))
        self.manipulate_yaml(jobid, gitrepo)
        start_dep_string = "kubectl apply -f worker_deployment.yaml"
        subprocess.check_output(start_dep_string.split())
        rescale_dep_string = "kubectl scale --replicas=" + str(nbr_workers_to_start) + " -f worker_deployment.yaml"
        subprocess.check_output(rescale_dep_string.split())
        self.reset_yaml(jobid, gitrepo)
        result_string = jobid + ":" + str(nbr_workers_to_start)
        self.channel.basic_publish(exchange='', routing_key=self.inprogress_queue, body=result_string)

    def manipulate_yaml(self, jobid, gitrepo):
        worker_id = "worker-" + str(jobid)
        rep_string_1 = 's/tobedecided/' + worker_id + '/g'
        sed_string_1 = "sed -i " + rep_string_1 + " worker_deployment.yaml"
        subprocess.check_output(sed_string_1.split())

        rep_string_2 = 's/jobholder/' + str(jobid) + '/g'
        sed_string_2 = "sed -i " + rep_string_2 + " worker_deployment.yaml"
        subprocess.check_output(sed_string_2.split())

        rep_string_3 = 's#gitrepoplace#' + str(gitrepo) + '#g'
        sed_string_3 = "sed -i " + rep_string_3 + " worker_deployment.yaml"
        subprocess.check_output(["sed", "-i", rep_string_3, "worker_deployment.yaml"])

    def reset_yaml(self, jobid, gitrepo):
        worker_id = "worker-" + str(jobid)
        rep_string_1 = 's/' + worker_id + '/tobedecided' + '/g'
        sed_string_1 = "sed -i " + rep_string_1 + " worker_deployment.yaml"
        subprocess.check_output(sed_string_1.split())

        rep_string_2 = 's/' + str(jobid) +'/jobholder'  + '/g'
        sed_string_2 = "sed -i " + rep_string_2 + " worker_deployment.yaml"
        subprocess.check_output(sed_string_2.split())

        rep_string_3 = 's#' + str(gitrepo) + '#gitrepoplace'  + '#g'
        sed_string_3 = "sed -i " + rep_string_3 + " worker_deployment.yaml"
        subprocess.check_output(["sed", "-i", rep_string_3, "worker_deployment.yaml"])


if __name__ == '__main__':
    master = Master()
    master.run()


