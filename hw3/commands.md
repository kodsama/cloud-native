# Setup kubectl
export KUBECONFIG=/path/to/file/group4-sandbox-admin.conf

# Helm charts
helm install stable/rabbitmq-ha --name rabbit-broker  -f rabbit/values.yaml
helm install stable/prometheus-operator --name prometheus-operator --namespace monitoring

# Upgrade
export ERLANGCOOKIE=$(kubectl get secrets -n rabbit rabbit-broker-rabbitmq-ha -o jsonpath="{.data.rabbitmq-erlang-cookie}" | base64 --decode)
helm upgrade --set rabbitmqErlangCookie=$ERLANGCOOKIE -f rabbit/values.yaml rabbit-broker stable/rabbitmq-ha

# Port forward
kubectl proxy
kubectl port-forward -n monitoring prometheus-prometheus-operator-prometheus-0 9090
kubectl port-forward rabbit-broker-rabbitmq-ha-0 15672
kubectl port-forward $(kubectl get pods --selector=app=grafana -n monitoring --output=jsonpath="{.items..metadata.name}") -n monitoring 3000

# Create temporary experimentation pod and delete
kubectl run -i --tty temp --image ubuntu:18.04
kubectl delete deployment temp

# Finding service names in namespace
kubectl get services -n namespace
# Adress for accessing a service in another namespace
servicename.namespace

# RabbitMQ tutorial for creating a worker queue in python
https://www.rabbitmq.com/tutorials/tutorial-two-python.html

# Chaos Monkey
https://github.com/helm/charts/tree/master/stable/chaoskube

# Find secret (rabbit mq example)
kubectl describe secrets 
# For rabbitmq check password, can also run for rabbitmq-username
kubectl get secret rabbit-broker-rabbitmq-ha -o jsonpath="{.data.rabbitmq-password}" | base64 -d

# Build, push, create secret and deploy flask app
sudo docker build -t <repo-url> .
sudo docker push <repo-url>
kubectl create secret docker-registry gitlab-regcred --docker-server=gitlab.datahub.erdc.ericsson.net:4567 --docker-username=<user> --docker-password=<token>
kubectl apply -f flask_deployment.yaml
kubectl replace -f flask_deployment.yaml
kubectl apply -f flask_service.yaml
# To access flask app on localhost:5000
kubectl port-forward <flask-pod-name> 5000

# Tommi
kubectl get pods
kubectl exec -it <worker-pod-name> bash
kubectl port-forward rabbit-broker-rabbitmq-ha-0 15672
kubectl port-forward <flask-pod-name> 5000
port run.sh in worker to python using pika, try to implement ack. 
sudo docker build -t gitlab.datahub.erdc.ericsson.net:4567/albheim/cloud-native/flaskapp_saas .
sudo docker push gitlab.datahub.erdc.ericsson.net:4567/albheim/cloud-native/flaskapp_saas

Try to implement show results in flask app
Redploy flask app
sudo systemctl start docker
sudo docker login gitlab.datahub.erdc.ericsson.net:4567
kubectl delete -f flask_deployment.yaml
kubectl apply -f flask_deployment.yaml

flask->docker innehåller allt för flask image
